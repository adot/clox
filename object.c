//
// Created by Nixon Enraght-Moony on 27/03/2021.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "object.h"

#include "memory.h"
#include "table.h"
#include "value.h"
#include "vm.h"

#define ALLOCATE_OBJ(type, objectType)                                         \
    (type*)allocateObject(sizeof(type), objectType)

static Obj* allocateObject(size_t size, ObjType type) {
    Obj* object  = (Obj*)reallocate(NULL, 0, size);
    object->type = type;

    object->next = vm.objects;
    vm.objects   = object;

    return object;
}

ObjFunction* newFunction() {
    ObjFunction* function = ALLOCATE_OBJ(ObjFunction, OBJ_FUNCTION);

    function->arity = 0;
    function->name  = NULL;
    initChunk(&function->chunk);

    return function;
}

ObjNative* newNative(NativeFn function) {
    ObjNative* native = ALLOCATE_OBJ(ObjNative, OBJ_NATIVE);
    native->function  = function;
    return native;
}

static struct ObjString* allocateString(char* chars, int lenght,
                                        uint32_t hash) {
    ObjString* string = ALLOCATE_OBJ(ObjString, OBJ_STRING);
    string->length    = lenght;
    string->chars     = chars;
    string->hash      = hash;

    tableSet(&vm.strings, string, NIL_VAL);

    return string;
}

static uint32_t hashString(const char* key, int length) {
    uint32_t hash = 2166136261u;

    for (int i = 0; i < length; i++) {
        hash ^= (uint8_t)key[i];
        hash *= 16777619;
    }

    return hash;
}

ObjString* takeString(char* chars, int length) {
    uint32_t hash = hashString(chars, length);

    ObjString* interned = tableFindString(&vm.strings, chars, length, hash);
    if (interned != NULL) {
        FREE_ARRAY(char, chars, length + 1);
        return interned;
    }

    return allocateString(chars, length, hash);
}

ObjString* copyString(const char* chars, int length) {
    uint32_t hash = hashString(chars, length);

    ObjString* interned = tableFindString(&vm.strings, chars, length, hash);
    if (interned != NULL)
        return interned;

    char* heapChars = ALLOCATE(char, length + 1);
    memcpy(heapChars, chars, length);
    heapChars[length] = '\0';

    return allocateString(heapChars, length, hash);
}

static void printFunction(FILE* stream, ObjFunction* function) {
    if (function->name == NULL) {
        fprintf(stream, "<script>");
        return;
    }
    fprintf(stream, "<fn %s>", function->name->chars);
}

void printObjectTo(Value value, FILE* stream) {
    switch (OBJ_TYPE(value)) {
        case OBJ_STRING: fprintf(stream, "\"%s\"", AS_CSTRING(value)); break;
        case OBJ_FUNCTION: printFunction(stream, AS_FUNCTION(value)); break;
        case OBJ_NATIVE: fprintf(stream, "<native fn>"); break;
        default:
            fprintf(stream, "[%s:%d] Unreachable\n", __FILE__, __LINE__);
            exit(1);
    }
}

void printObject(Value value) {
    printObjectTo(value, stdout);
}