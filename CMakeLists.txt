cmake_minimum_required(VERSION 3.20)
project(clox2 C)

set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -g -Wall -Wextra -Wno-unused-parameter -fdiagnostics-color")

set (CMAKE_C_STANDARD 11)

set(CLOX_DEBUG_CODE off CACHE BOOL "Print Bytecode")
set(CLOX_DEBUG_EXECUTION off CACHE BOOL "Print execution")

if(CLOX_DEBUG_CODE)
        add_compile_options("-DDEBUG_PRINT_CODE")
endif()
if(CLOX_DEBUG_EXECUTION)
        add_compile_options("-DDEBUG_TRACE_EXECUTION")
endif()

add_executable(clox2
        main.c common.c common.h chunk.c chunk.h memory.c memory.h
        debug.c debug.h value.c value.h vm.c vm.h compiler.c
        compiler.h scanner.c scanner.h object.c object.h table.c table.h
        linenoise/linenoise.h linenoise/linenoise.c)