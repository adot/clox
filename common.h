//
// Created by Nixon Enraght-Moony on 26/03/2021.
//

#ifndef CLOX2_COMMON_H
#define CLOX2_COMMON_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

// #define DEBUG_TRACE_EXECUTION
// #define DEBUG_PRINT_CODE

#define UINT8_COUNT (UINT8_MAX + 1)

#endif // CLOX2_COMMON_H
