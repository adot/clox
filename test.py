#!/usr/bin/env python3

import os
import glob
import threading

os.system("ninja -C build")

ts = []

for i in glob.glob("tests/**/*.lox", recursive=True):
    print(i)
    ts.append(i)
    os.system(f"build/clox2 {i} > {i.replace('lox', 'out')}") == 0
