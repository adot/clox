//
// Created by Nixon Enraght-Moony on 26/03/2021.
//

#ifndef CLOX2_COMPILER_H
#define CLOX2_COMPILER_H

// Not IWYU, not sure why.
#include "object.h"
#include "vm.h"

ObjFunction* compile(const char* source);

#endif // CLOX2_COMPILER_H
