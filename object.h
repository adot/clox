//
// Created by Nixon Enraght-Moony on 27/03/2021.
//

#ifndef CLOX2_OBJECT_H
#define CLOX2_OBJECT_H

#include <stdio.h>

#include "chunk.h"
#include "common.h"
#include "value.h"

#define OBJ_TYPE(value)    (AS_OBJ(value)->type)
#define IS_STRING(value)   isObjType(value, OBJ_STRING)
#define IS_FUNCTION(value) isObjType(value, OBJ_FUNCTION)
#define IS_NATIVE(value)   isObjType(value, OBJ_NATIVE)

#define AS_FUNCTION(value) ((ObjFunction*)AS_OBJ(value))
#define AS_NATIVE(value)   (((ObjNative*)AS_OBJ(value))->function)
#define AS_STRING(value)   ((ObjString*)AS_OBJ(value))
#define AS_CSTRING(value)  (((ObjString*)AS_OBJ(value))->chars)

typedef enum
{
    OBJ_STRING,
    OBJ_NATIVE,
    OBJ_FUNCTION,
} ObjType;

struct Obj {
    ObjType     type;
    struct Obj* next;
};

typedef struct {
    Obj        obj;
    int        arity;
    Chunk      chunk;
    ObjString* name;
} ObjFunction;

typedef Value (*NativeFn)(int argCount, Value* args);

typedef struct {
    Obj      obj;
    NativeFn function;
} ObjNative;

struct ObjString {
    Obj      obj;
    int      length;
    char*    chars;
    uint32_t hash;
};

ObjFunction* newFunction();
ObjNative*   newNative();

ObjString* takeString(char* chars, int length);
ObjString* copyString(const char* chars, int lenght);
void       printObject(Value value);
void       printObjectTo(Value value, FILE* stream);

static inline bool isObjType(Value value, ObjType type) {
    return IS_OBJ(value) && AS_OBJ(value)->type == type;
}

#endif // CLOX2_OBJECT_H
